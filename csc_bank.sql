-- MySQL dump 10.13  Distrib 5.6.22, for Win64 (x86_64)
--
-- Host: localhost    Database: csc_banking
-- ------------------------------------------------------
-- Server version	5.6.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `login_id` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `id_card_number` varchar(12) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `midname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) NOT NULL,
  `phone_no_1` varchar(12) NOT NULL,
  `phone_no_2` varchar(12) DEFAULT NULL,
  `email_1` varchar(45) NOT NULL,
  `email_2` varchar(45) DEFAULT NULL,
  `address_1` varchar(45) NOT NULL,
  `address_2` varchar(45) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  PRIMARY KEY (`login_id`),
  KEY `account_role_idx` (`role_id`),
  KEY `account_state_idx` (`status_id`),
  CONSTRAINT `account_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `account_state` FOREIGN KEY (`status_id`) REFERENCES `status` (`status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES ('nctam','123','3214939070','Tam','Chi','Nguyen','01686022294','01686022293','nctam109@gmail.com','nctam@outlook.com','Tan Binh','Di An',1,2);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_amount`
--

DROP TABLE IF EXISTS `consumer_amount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_amount` (
  `account_number` varchar(13) NOT NULL,
  `login_id` varchar(30) NOT NULL,
  `fund` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_number`),
  KEY `amount_account_idx` (`login_id`),
  CONSTRAINT `amount_account_login_id` FOREIGN KEY (`login_id`) REFERENCES `account` (`login_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_amount`
--

LOCK TABLES `consumer_amount` WRITE;
/*!40000 ALTER TABLE `consumer_amount` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_amount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_tracsaction`
--

DROP TABLE IF EXISTS `consumer_tracsaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_tracsaction` (
  `tracsaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `source_account_number` varchar(13) NOT NULL,
  `target_account_number` varchar(13) NOT NULL,
  `amount` varchar(45) NOT NULL,
  `transaction_date` date NOT NULL,
  `begin_time` time NOT NULL,
  `end_time` time NOT NULL,
  PRIMARY KEY (`tracsaction_id`),
  KEY `trans_source_number_idx` (`source_account_number`),
  KEY `trans_target_number_idx` (`target_account_number`),
  CONSTRAINT `trans_source_number` FOREIGN KEY (`source_account_number`) REFERENCES `consumer_amount` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trans_target_number` FOREIGN KEY (`target_account_number`) REFERENCES `consumer_amount` (`account_number`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_tracsaction`
--

LOCK TABLES `consumer_tracsaction` WRITE;
/*!40000 ALTER TABLE `consumer_tracsaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_tracsaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumer_transaction`
--

DROP TABLE IF EXISTS `consumer_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumer_transaction` (
  `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `begin_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `transaction_date` datetime DEFAULT NULL,
  `source_account_number` varchar(255) DEFAULT NULL,
  `target_account_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `FK_77knqd9pfik18bfv4i02udvsu` (`source_account_number`),
  KEY `FK_9vi19a2rnklrex00yc385n4ho` (`target_account_number`),
  CONSTRAINT `FK_77knqd9pfik18bfv4i02udvsu` FOREIGN KEY (`source_account_number`) REFERENCES `consumner_amount` (`account_number`),
  CONSTRAINT `FK_9vi19a2rnklrex00yc385n4ho` FOREIGN KEY (`target_account_number`) REFERENCES `consumner_amount` (`account_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumer_transaction`
--

LOCK TABLES `consumer_transaction` WRITE;
/*!40000 ALTER TABLE `consumer_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumer_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consumner_amount`
--

DROP TABLE IF EXISTS `consumner_amount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `consumner_amount` (
  `account_number` varchar(255) NOT NULL,
  `fund` double DEFAULT NULL,
  `login_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`account_number`),
  KEY `FK_s0mp7u2ovy5tj37t2hbwm80o4` (`login_id`),
  CONSTRAINT `FK_s0mp7u2ovy5tj37t2hbwm80o4` FOREIGN KEY (`login_id`) REFERENCES `account` (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consumner_amount`
--

LOCK TABLES `consumner_amount` WRITE;
/*!40000 ALTER TABLE `consumner_amount` DISABLE KEYS */;
/*!40000 ALTER TABLE `consumner_amount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_transaction`
--

DROP TABLE IF EXISTS `history_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_transaction` (
  `history_transaction_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_impact` varchar(30) NOT NULL,
  `account_effect` varchar(30) NOT NULL,
  `command` varchar(10) DEFAULT NULL,
  `history_transaction_date` datetime NOT NULL,
  `history_transaction_note` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`history_transaction_id`),
  KEY `trans_source_account_idx` (`account_impact`),
  KEY `trans_target_account_idx` (`account_effect`),
  CONSTRAINT `trans_source_account` FOREIGN KEY (`account_impact`) REFERENCES `account` (`login_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `trans_target_account` FOREIGN KEY (`account_effect`) REFERENCES `account` (`login_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='							';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_transaction`
--

LOCK TABLES `history_transaction` WRITE;
/*!40000 ALTER TABLE `history_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'USER_ADMIN','Permisstion for user admin'),(2,'USER_SUPPORT','Permisstion for user support'),(3,'ACCOUNT_ADMIN','Permisstion for account admin'),(4,'ACCOUNT_SUPPORT','Permisstion for account support'),(5,'REPORT_SUPORT','Permisstion for report support'),(6,'CONSUMER','Permisstion for consumer');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `status` (
  `status_id` int(11) NOT NULL,
  `status_name` varchar(10) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'new'),(2,'active'),(3,'disable'),(4,'removeable');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-13 12:55:01
