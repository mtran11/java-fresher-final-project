package csc.fresher.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "history_transaction")
public class HistoryTransaction implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column (name = "history_transaction_id")
	private int historyTransactionId;
	
	@ManyToOne
	@JoinColumn (name = "account_impact")
	private Account accountImpact;
	
	@ManyToOne
	@JoinColumn (name = "account_effect")
	private Account accountEffect;
	
	@Column (name = "command")
	private String command;
	
	@Column (name = "history_transaction_date")
	private Date historyTransactionDate;
	
	@Column (name = "history_transaction_note")
	private String historyTransactionNote;
	
	public HistoryTransaction() {
		super();
	}

	/**
	 * @param historyTransactionId
	 * @param accountImpact
	 * @param accountEffect
	 * @param command
	 * @param historyTransactionDate
	 * @param historyTransactionNote
	 */
	public HistoryTransaction(int historyTransactionId, Account accountImpact,
			Account accountEffect, String command, Date historyTransactionDate,
			String historyTransactionNote) {
		super();
		this.historyTransactionId = historyTransactionId;
		this.accountImpact = accountImpact;
		this.accountEffect = accountEffect;
		this.command = command;
		this.historyTransactionDate = historyTransactionDate;
		this.historyTransactionNote = historyTransactionNote;
	}

	/**
	 * @return the historyTransactionId
	 */
	public int getHistoryTransactionId() {
		return historyTransactionId;
	}

	/**
	 * @param historyTransactionId the historyTransactionId to set
	 */
	public void setHistoryTransactionId(int historyTransactionId) {
		this.historyTransactionId = historyTransactionId;
	}

	/**
	 * @return the accountImpact
	 */
	public Account getAccountImpact() {
		return accountImpact;
	}

	/**
	 * @param accountImpact the accountImpact to set
	 */
	public void setAccountImpact(Account accountImpact) {
		this.accountImpact = accountImpact;
	}

	/**
	 * @return the accountEffect
	 */
	public Account getAccountEffect() {
		return accountEffect;
	}

	/**
	 * @param accountEffect the accountEffect to set
	 */
	public void setAccountEffect(Account accountEffect) {
		this.accountEffect = accountEffect;
	}

	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command the command to set
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the historyTransactionDate
	 */
	public Date getHistoryTransactionDate() {
		return historyTransactionDate;
	}

	/**
	 * @param historyTransactionDate the historyTransactionDate to set
	 */
	public void setHistoryTransactionDate(Date historyTransactionDate) {
		this.historyTransactionDate = historyTransactionDate;
	}

	/**
	 * @return the historyTransactionNote
	 */
	public String getHistoryTransactionNote() {
		return historyTransactionNote;
	}

	/**
	 * @param historyTransactionNote the historyTransactionNote to set
	 */
	public void setHistoryTransactionNote(String historyTransactionNote) {
		this.historyTransactionNote = historyTransactionNote;
	}
	
}
