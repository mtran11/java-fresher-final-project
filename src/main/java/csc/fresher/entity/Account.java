package csc.fresher.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "login_id")
	private String loginId;

	@Column(name = "password")
	private String password;

	@Column(name = "id_card_number")
	private String idCardNumber;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "midname")
	private String midName;

	@Column(name = "lastname")
	private String lastName;

	@Column(name = "phone_no_1")
	private String firstPhoneNumber;

	@Column(name = "phone_no_2")
	private String secondPhoneNumber;

	@Column(name = "email_1")
	private String firstEmail;

	@Column(name = "email_2")
	private String secondEmail;

	@Column(name = "address_1")
	private String firstAddress;

	@Column(name = "address_2")
	private String secondAddress;

	@ManyToOne
	@JoinColumn(name = "role_id", referencedColumnName = "role_id")
	private Role role;

	@ManyToOne
	@JoinColumn(name = "status_id", referencedColumnName = "status_id")
	private Status status;

	@OneToMany(mappedBy = "accountImpact")
	private Collection<HistoryTransaction> accountImpact;

	@OneToMany(mappedBy = "accountEffect")
	private Collection<HistoryTransaction> accountEffect;

	public Account() {
		super();
	}

	/**
	 * @param loginId
	 * @param password
	 * @param idCardNumber
	 * @param firstName
	 * @param midName
	 * @param lastName
	 * @param firstPhoneNumber
	 * @param secondPhoneNumber
	 * @param firstEmail
	 * @param secondEmail
	 * @param firstAddress
	 * @param secondAddress
	 * @param role
	 * @param status
	 * @param accountImpact
	 * @param accountEffect
	 */
	public Account(String loginId, String password, String idCardNumber,
			String firstName, String midName, String lastName,
			String firstPhoneNumber, String secondPhoneNumber,
			String firstEmail, String secondEmail, String firstAddress,
			String secondAddress, Role role, Status status,
			Collection<HistoryTransaction> accountImpact,
			Collection<HistoryTransaction> accountEffect) {
		super();
		this.loginId = loginId;
		this.password = password;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.midName = midName;
		this.lastName = lastName;
		this.firstPhoneNumber = firstPhoneNumber;
		this.secondPhoneNumber = secondPhoneNumber;
		this.firstEmail = firstEmail;
		this.secondEmail = secondEmail;
		this.firstAddress = firstAddress;
		this.secondAddress = secondAddress;
		this.role = role;
		this.status = status;
		this.accountImpact = accountImpact;
		this.accountEffect = accountEffect;
	}

	public Account(String loginId, String password, String idCardNumber,
			String firstName, String midName, String lastName,
			String firstPhoneNumber, String secondPhoneNumber,
			String firstEmail, String secondEmail, String firstAddress,
			String secondAddress, Role role, Status status) {
		super();
		this.loginId = loginId;
		this.password = password;
		this.idCardNumber = idCardNumber;
		this.firstName = firstName;
		this.midName = midName;
		this.lastName = lastName;
		this.firstPhoneNumber = firstPhoneNumber;
		this.secondPhoneNumber = secondPhoneNumber;
		this.firstEmail = firstEmail;
		this.secondEmail = secondEmail;
		this.firstAddress = firstAddress;
		this.secondAddress = secondAddress;
		this.role = role;
		this.status = status;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId
	 *            the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the idCardNumber
	 */
	public String getIdCardNumber() {
		return idCardNumber;
	}

	/**
	 * @param idCardNumber
	 *            the idCardNumber to set
	 */
	public void setIdCardNumber(String idCardNumber) {
		this.idCardNumber = idCardNumber;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the midName
	 */
	public String getMidName() {
		return midName;
	}

	/**
	 * @param midName
	 *            the midName to set
	 */
	public void setMidName(String midName) {
		this.midName = midName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the firstPhoneNumber
	 */
	public String getFirstPhoneNumber() {
		return firstPhoneNumber;
	}

	/**
	 * @param firstPhoneNumber
	 *            the firstPhoneNumber to set
	 */
	public void setFirstPhoneNumber(String firstPhoneNumber) {
		this.firstPhoneNumber = firstPhoneNumber;
	}

	/**
	 * @return the secondPhoneNumber
	 */
	public String getSecondPhoneNumber() {
		return secondPhoneNumber;
	}

	/**
	 * @param secondPhoneNumber
	 *            the secondPhoneNumber to set
	 */
	public void setSecondPhoneNumber(String secondPhoneNumber) {
		this.secondPhoneNumber = secondPhoneNumber;
	}

	/**
	 * @return the firstEmail
	 */
	public String getFirstEmail() {
		return firstEmail;
	}

	/**
	 * @param firstEmail
	 *            the firstEmail to set
	 */
	public void setFirstEmail(String firstEmail) {
		this.firstEmail = firstEmail;
	}

	/**
	 * @return the secondEmail
	 */
	public String getSecondEmail() {
		return secondEmail;
	}

	/**
	 * @param secondEmail
	 *            the secondEmail to set
	 */
	public void setSecondEmail(String secondEmail) {
		this.secondEmail = secondEmail;
	}

	/**
	 * @return the firstAddress
	 */
	public String getFirstAddress() {
		return firstAddress;
	}

	/**
	 * @param firstAddress
	 *            the firstAddress to set
	 */
	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}

	/**
	 * @return the secondAddress
	 */
	public String getSecondAddress() {
		return secondAddress;
	}

	/**
	 * @param secondAddress
	 *            the secondAddress to set
	 */
	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}

	/**
	 * @return the accountImpact
	 */
	public Collection<HistoryTransaction> getAccountImpact() {
		return accountImpact;
	}

	/**
	 * @param accountImpact
	 *            the accountImpact to set
	 */
	public void setAccountImpact(Collection<HistoryTransaction> accountImpact) {
		this.accountImpact = accountImpact;
	}

	/**
	 * @return the accountEffect
	 */
	public Collection<HistoryTransaction> getAccountEffect() {
		return accountEffect;
	}

	/**
	 * @param accountEffect
	 *            the accountEffect to set
	 */
	public void setAccountEffect(Collection<HistoryTransaction> accountEffect) {
		this.accountEffect = accountEffect;
	}

}
