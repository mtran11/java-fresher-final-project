package csc.fresher.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "consumner_amount")
public class ConSumerAmount implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column (name = "account_number")
	private String accountNumber;
	
	@Column (name = "fund")
	private double fund;
	
	@ManyToOne
	@JoinColumn (name = "login_id")
	private Account account;
	
	@OneToMany (mappedBy = "sourceAccountNumber")
	private Collection <ConsumerTransaction> sourceAccountNumber;
	
	@OneToMany (mappedBy = "targetAccountNumber")
	private Collection <ConsumerTransaction> targetAccountNumber;
	
	public ConSumerAmount() {
		super();
	}

	/**
	 * @param accountNumber
	 * @param fund
	 * @param account
	 * @param sourceAccountNumber
	 * @param targetAccountNumber
	 */
	public ConSumerAmount(String accountNumber, double fund, Account account,
			Collection<ConsumerTransaction> sourceAccountNumber,
			Collection<ConsumerTransaction> targetAccountNumber) {
		super();
		this.accountNumber = accountNumber;
		this.fund = fund;
		this.account = account;
		this.sourceAccountNumber = sourceAccountNumber;
		this.targetAccountNumber = targetAccountNumber;
	}

	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * @return the fund
	 */
	public double getFund() {
		return fund;
	}

	/**
	 * @param fund the fund to set
	 */
	public void setFund(double fund) {
		this.fund = fund;
	}

	/**
	 * @return the account
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(Account account) {
		this.account = account;
	}

	/**
	 * @return the sourceAccountNumber
	 */
	public Collection<ConsumerTransaction> getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	/**
	 * @param sourceAccountNumber the sourceAccountNumber to set
	 */
	public void setSourceAccountNumber(
			Collection<ConsumerTransaction> sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	/**
	 * @return the targetAccountNumber
	 */
	public Collection<ConsumerTransaction> getTargetAccountNumber() {
		return targetAccountNumber;
	}

	/**
	 * @param targetAccountNumber the targetAccountNumber to set
	 */
	public void setTargetAccountNumber(
			Collection<ConsumerTransaction> targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}
	
}
