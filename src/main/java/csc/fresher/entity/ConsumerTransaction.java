package csc.fresher.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name = "consumer_transaction")
public class ConsumerTransaction implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue (strategy = GenerationType.AUTO)
	@Column (name = "transaction_id")
	private int transactionId;
	
	@ManyToOne
	@JoinColumn (name = "source_account_number")
	private ConSumerAmount sourceAccountNumber;
	
	@ManyToOne
	@JoinColumn (name = "target_account_number")
	private ConSumerAmount targetAccountNumber;
	
	@Column (name = "amount")
	private double amount;
	
	@Column (name = "transaction_date")
	private Date transactionDate;
	
	@Column (name = "begin_time")
	private Time beginTime;
	
	@Column (name = "end_time")
	private Time endTime;
	
	public ConsumerTransaction() {
		super();
	}

	/**
	 * @param transactionId
	 * @param sourceAccountNumber
	 * @param targetAccountNumber
	 * @param amount
	 * @param transactionDate
	 * @param beginTime
	 * @param endTime
	 */
	public ConsumerTransaction(int transactionId, ConSumerAmount sourceAccountNumber,
			ConSumerAmount targetAccountNumber, double amount, Date transactionDate,
			Time beginTime, Time endTime) {
		super();
		this.transactionId = transactionId;
		this.sourceAccountNumber = sourceAccountNumber;
		this.targetAccountNumber = targetAccountNumber;
		this.amount = amount;
		this.transactionDate = transactionDate;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	/**
	 * @return the transactionId
	 */
	public int getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the sourceAccountNumber
	 */
	public ConSumerAmount getSourceAccountNumber() {
		return sourceAccountNumber;
	}

	/**
	 * @param sourceAccountNumber the sourceAccountNumber to set
	 */
	public void setSourceAccountNumber(ConSumerAmount sourceAccountNumber) {
		this.sourceAccountNumber = sourceAccountNumber;
	}

	/**
	 * @return the targetAccountNumber
	 */
	public ConSumerAmount getTargetAccountNumber() {
		return targetAccountNumber;
	}

	/**
	 * @param targetAccountNumber the targetAccountNumber to set
	 */
	public void setTargetAccountNumber(ConSumerAmount targetAccountNumber) {
		this.targetAccountNumber = targetAccountNumber;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * @return the transactionDate
	 */
	public Date getTransactionDate() {
		return transactionDate;
	}

	/**
	 * @param transactionDate the transactionDate to set
	 */
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	/**
	 * @return the beginTime
	 */
	public Time getBeginTime() {
		return beginTime;
	}

	/**
	 * @param beginTime the beginTime to set
	 */
	public void setBeginTime(Time beginTime) {
		this.beginTime = beginTime;
	}

	/**
	 * @return the endTime
	 */
	public Time getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Time endTime) {
		this.endTime = endTime;
	}
	
}
