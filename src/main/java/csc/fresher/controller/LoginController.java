package csc.fresher.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)	
	public String goLogin(){
		return "login";
	}

/*	@RequestMapping(value = "/useradmin", method = RequestMethod.GET)	
	public String goAdmin(){
		return "useradmin";
	}*/
}
