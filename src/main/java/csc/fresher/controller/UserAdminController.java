package csc.fresher.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import csc.fresher.constant.Constant;
import csc.fresher.dao.AccountDAO;
import csc.fresher.entity.Account;
import csc.fresher.entity.Role;
import csc.fresher.entity.Status;

@Controller
public class UserAdminController {
	
	AccountDAO accountDao = new AccountDAO();
	
	
	@RequestMapping(value = "/useradmin")
	public String getAccountList(Model model) {
		List<Account> accountList = accountDao.getAccountNew();
		model.addAttribute("listAccountNew", accountList);

		List<Account> accountList2 = accountDao.getAccountUpdate();
		model.addAttribute("listAccountUpdate", accountList2);
/*		AccountDAO accountDAO = new AccountDAO();
		List<Role> roles = accountDAO.getRoles();
		model.addAttribute("listRole", roles);
		
		List<Status> status = accountDAO.getStatus();
		model.addAttribute("listStatus", status);*/
		return ("useradmin");
	}
	
	@RequestMapping(value = "/acceptAccount", method= RequestMethod.POST, params = {"accept", "!reject"})
	public String acceptAccount(HttpServletRequest request, Model model) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String cardNumber = request.getParameter("idCardNumber");
		String firstName = request.getParameter("firstName");
		String midName = request.getParameter("midName");
		String lastName = request.getParameter("lastName");
		String firstPhoneNumber = request.getParameter("firstPhoneNumber");
		String secondPhoneNumber = request.getParameter("secondPhoneNumber");
		String firstEmail = request.getParameter("firstEmail");
		String secondEmail = request.getParameter("secondEmail");
		String firstAddress = request.getParameter("firstAddress");
		String secondAddress = request.getParameter("secondAddress");
		String role = request.getParameter("role");

		Role roles = accountDao.findRole(Integer.parseInt(role));
		Status status = accountDao.findStatus(Constant.STATUS_ID_ACTIVE);
		/*System.out.println(loginId+password+cardNumber+firstName+midName+lastName+
				firstPhoneNumber+secondPhoneNumber+firstEmail+secondEmail+firstAddress+secondAddress+
				roles+ status);*/
		Account account = new Account(loginId,password,cardNumber,firstName,midName,lastName,
				firstPhoneNumber,secondPhoneNumber,firstEmail,secondEmail,firstAddress,secondAddress,
				roles, status);
		accountDao.updateAccount(account);
		
		return "forward:/useradmin.html";
	}
	
	@RequestMapping(value = "/acceptAccount", method= RequestMethod.POST, params = {"!accept", "reject"})
	public String rejectAccount(HttpServletRequest request, Model model) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String cardNumber = request.getParameter("idCardNumber");
		String firstName = request.getParameter("firstName");
		String midName = request.getParameter("midName");
		String lastName = request.getParameter("lastName");
		String firstPhoneNumber = request.getParameter("firstPhoneNumber");
		String secondPhoneNumber = request.getParameter("secondPhoneNumber");
		String firstEmail = request.getParameter("firstEmail");
		String secondEmail = request.getParameter("secondEmail");
		String firstAddress = request.getParameter("firstAddress");
		String secondAddress = request.getParameter("secondAddress");
		String role = request.getParameter("role");

		Role roles = accountDao.findRole(Integer.parseInt(role));
		Status status = accountDao.findStatus(Constant.STATUS_ID_DISABLE);
		Account account = new Account(loginId,password,cardNumber,firstName,midName,lastName,
				firstPhoneNumber,secondPhoneNumber,firstEmail,secondEmail,firstAddress,secondAddress,
				roles, status);
		accountDao.updateAccount(account);
		
		return "forward:/useradmin.html";
	}
		
}
