package csc.fresher.controller;

import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import csc.fresher.constant.Constant;
import csc.fresher.dao.AccountDAO;
import csc.fresher.entity.Account;
import csc.fresher.entity.Role;
import csc.fresher.entity.Status;

@Controller
public class UserSupportController {

	@RequestMapping(value = "/usersupport")
	public String getAccountList(Model model) {
		// Create a new AccountDAO
		// System.out.println("here");

		AccountDAO accountDao = new AccountDAO();
		// System.out.println("here1");
		// Get the list of all accounts from DB
		List<Account> accountList = accountDao.getAccounts();

		// Add the list of accounts to request object which will be then used in
		// jsp to show to user
		// for (int i = 0; i < 1; i++) {
		// System.out.println(accountList + "1");
		// }
		model.addAttribute("listAccount", accountList);
		// System.out.println(model);

		AccountDAO accountDAO = new AccountDAO();
		List<Role> roles = accountDAO.getRoles();
		model.addAttribute("listRole", roles);
		
		List<Status> status = accountDAO.getStatus();
		model.addAttribute("listStatus", status);
		return ("usersupport");
	}

	@RequestMapping(value = "/createAccount")
	public String addAccount(HttpServletRequest request, Model model) {
		// Read account info from request and save into Account object
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String idCardNumber = request.getParameter("idCardNumber");
		String firstName = request.getParameter("firstName");
		String midName = request.getParameter("midName");
		String lastName = request.getParameter("lastName");
		String firstPhoneNumber = request.getParameter("firstPhoneNumber");
		String secondPhoneNumber = request.getParameter("secondPhoneNumber");
		String firstEmail = request.getParameter("firstEmail");
		String secondEmail = request.getParameter("secondEmail");
		String firstAddress = request.getParameter("firstAddress");
		String secondAddress = request.getParameter("secondAddress");
		String role = request.getParameter("role");
		/*String status = request.getParameter("status");*/
		
		/*Validation Account*/
		String errorMessage = "";
		/*Login Name*/
		if ((loginId == null) || (loginId.length() >= 15) || (loginId.length() <= 6)) {
			errorMessage = "LoginId should not be empty and length between from 6 to 15";
			/*model.addAttribute("errorLogin", errorMessage);*/
			request.getSession().setAttribute("errorLogin", errorMessage);
			return "usersupport";
		}
		request.getSession().setAttribute("errorLogin", "");
		/*Password*/
		if ((password == null) || (password.length() >= 45) || (password.length() <= 8)) {
			errorMessage = "Password should not be empty and length between from 8 to 45";
			model.addAttribute("errorPassword", errorMessage);
		}
		
		/*Card Number*/
		boolean idCard= Pattern.matches("[0-9]",idCardNumber);
		if ((idCard == false) || (idCardNumber.length() <= 9) || (idCardNumber.length() >= 12)) {			
			errorMessage = "Id Card Number should be numeric character only and length between from 10 to 11";
			model.addAttribute("errorCardNumber", errorMessage);
		}
		
		/*First Name*/
		boolean fname = Pattern.matches("[a-zA-Z]",firstName);
		if (fname == false) {			
			errorMessage = "First name must not be empty";
			model.addAttribute("errorFName", errorMessage);
		}
		
		/*Mid Name*/
		/*boolean mname = Pattern.matches("[a-zA-Z]",midName);
		if (mname == false) {			
			errorMessage = "Mid name must not be empty";
			model.addAttribute("errorMName", errorMessage);
		}*/
		
		/*Last Name*/
		boolean lname = Pattern.matches("[a-zA-Z]",lastName);
		if (lname == false) {			
			errorMessage = "Last name must not be empty";
			model.addAttribute("errorLName", errorMessage);
		}
		
		/*Phone Number*/
		boolean fphone = Pattern.matches("[0][0-9]{9}",firstPhoneNumber); //{9} is accept 10 digits
		if ((fphone == false) /*|| (firstPhoneNumber.length() != 10) || (firstPhoneNumber.length() != 11)*/) {			
			errorMessage = "Phone Number should be numeric character only and length is 10";
			model.addAttribute("errorFPhone", errorMessage);
		}
		
		/*Email*/
		boolean femail = Pattern.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",firstPhoneNumber);
		if ((femail == false) || (firstPhoneNumber.length() <= 9) || (firstPhoneNumber.length() >= 12)) {			
			errorMessage = "Email should like example@email.com";
			model.addAttribute("errorFMail", errorMessage);
		}
		
		/*Address*/
		boolean faddress = Pattern.matches("[a-zA-Z0-9]",firstAddress);
		if (faddress == false) {			
			errorMessage = "First Address must not be empty";
			model.addAttribute("errorFAddress", errorMessage);
		}
		
		AccountDAO accountDao = new AccountDAO();
		Role roles = accountDao.findRole(Integer.parseInt(role));
		Status statuss = accountDao.findStatus(Constant.STATUS_ID_NEW);
		Account account = new Account(loginId, password, idCardNumber,
				firstName, midName, lastName, firstPhoneNumber,
				secondPhoneNumber, firstEmail, secondEmail, firstAddress,
				secondAddress, roles, statuss);

		accountDao.addAccount(account);
		/*model.addAttribute("roleList", accountDao.getRoles());
		model.addAttribute("listStatus", accountDao.getStatus());*/
		return "usersupport";
	}
	
	

	@RequestMapping(value = "/getAccountDetail", method = RequestMethod.POST)
	public @ResponseBody Account getAccountDetail(@RequestParam String loginId) {
		
		AccountDAO accountDAO = new AccountDAO();
		
		Account acc= accountDAO.getAccountByLoginId(loginId);
//		System.out.println(acc.getLoginId());
		return acc;
	}
	
	@RequestMapping(value = "/updateAccount")
	public String editCustomer(HttpServletRequest request, Model model) {

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String cardNumber = request.getParameter("idCardNumber");
		String firstName = request.getParameter("firstName");
		String midName = request.getParameter("midName");
		String lastName = request.getParameter("lastName");
		String firstPhoneNumber = request.getParameter("firstPhoneNumber");
		String secondPhoneNumber = request.getParameter("secondPhoneNumber");
		String firstEmail = request.getParameter("firstEmail");
		String secondEmail = request.getParameter("secondEmail");
		String firstAddress = request.getParameter("firstAddress");
		String secondAddress = request.getParameter("secondAddress");
		String role = request.getParameter("role");

		AccountDAO dao = new AccountDAO();
		Role roles = dao.findRole(Integer.parseInt(role));
		Status status = dao.findStatus(Constant.STATUS_ID_DISABLE);
		
		Account account = new Account(loginId,password,cardNumber,firstName,midName,lastName,
				firstPhoneNumber,secondPhoneNumber,firstEmail,secondEmail,firstAddress,secondAddress,
				roles, status);
		dao.updateAccount(account);
		
/*		boolean isSeccess = dao.updateAccount(account);
		if (isSeccess) {
			return "redirect:usersupport.html";
		}*/
		return "forward:/usersupport.html";
	}

}
