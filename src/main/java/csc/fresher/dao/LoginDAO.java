package csc.fresher.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import csc.fresher.constant.Constant;
import csc.fresher.entity.Account;

@Repository
public class LoginDAO {

	@PersistenceContext
	EntityManager em;

	@Transactional
	public Account checkLogin(String loginId, String password) {
		Account account = em.find(Account.class, loginId);
		if (null != account && account.getPassword().equals(password)) {
			return account;
		}
		return null;
	}
	
	public List<Account> getListAccountNew() {
		TypedQuery<Account> query = em.createQuery(
				"SELECT t FROM SystemAccount t WHERE t.systemStatus.statusId  = "
						+ Constant.STATUS_ID_NEW + "", Account.class);
		return query.getResultList();
	}
	
}
