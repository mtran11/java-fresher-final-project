package csc.fresher.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;

import csc.fresher.constant.Constant;
import csc.fresher.controller.EntityManagerFactoryUtil;
import csc.fresher.entity.Account;
import csc.fresher.entity.Role;
import csc.fresher.entity.Status;

@Component
public class AccountDAO {
	/**
	 * @return all accounts from Account table
	 */
	public List<Account> getAccounts() {

		// Obtains entity manager object
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();
		// System.out.println("d");

		// -----------Begin transaction-----------
		List<Account> accounts = null;
		// System.out.println("da");

		try {
			entr.begin();
			// Get a list of accounts from DB
			// System.out.println("dao");
			TypedQuery<Account> query = entityManager.createQuery(
					"SELECT a FROM Account a", Account.class);

			// System.out.println(query);
			accounts = query.getResultList();

			// System.out.println("dao2");
			entr.commit();

			// System.out.println(accounts);
		} catch (Exception e) {
			entityManager.close();
		}
		// -----------End transaction-----------

		return accounts;
	}

	public List<Account> getAccountNew() {

		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		EntityTransaction entr = entityManager.getTransaction();

		List<Account> accounts = null;

		try {
			entr.begin();
			TypedQuery<Account> query = entityManager
					.createQuery(
							"SELECT a FROM Account a WHERE a.status.statusId = :STATUS_ID_NEW ",
							Account.class).setParameter("STATUS_ID_NEW",
							Constant.STATUS_ID_NEW);
			/*"SELECT a FROM Account a WHERE a.status.statusId = 1" // true
			 * "SELECT a FROM Account a WHERE a.status.statusId =: STATUS_ID_NEW", //false at =:
			 * Account.class).setParameter("STATUS_ID_NEW",
			 * Constant.STATUS_ID_NEW);
			 */

			accounts = query.getResultList();
			entr.commit();

		} catch (Exception e) {
			entityManager.close();
		}
		return accounts;
	}
	
	public List<Account> getAccountUpdate() {

		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		EntityTransaction entr = entityManager.getTransaction();

		List<Account> accounts = null;

		try {
			entr.begin();
			TypedQuery<Account> query = entityManager
					.createQuery(
							"SELECT a FROM Account a WHERE a.status.statusId = :STATUS_ID_DISABLE ",
							Account.class).setParameter("STATUS_ID_DISABLE",
							Constant.STATUS_ID_DISABLE);
			accounts = query.getResultList();
			entr.commit();

		} catch (Exception e) {
			entityManager.close();
		}
		return accounts;
	}

	public boolean addAccount(Account account) {
		// Obtains entity manager object
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();

		try {
			// -----------Begin transaction-----------
			entr.begin();
			// Insert a row to Account table
			entityManager.persist(account);
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
		}
		// -----------End transaction-----------

		return true;
	}

	public Role findRole(int role_id) {
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();
		entr.begin();

		Role role = entityManager.find(Role.class, role_id);
		entr.commit();

		return role;
	}

	public Role findRoleName(String roleName) {
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();
		entr.begin();

		Role role = entityManager.find(Role.class, roleName);
		entr.commit();

		return role;
	}

	public List<Role> getRoles() {

		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		EntityTransaction entr = entityManager.getTransaction();
		List<Role> roles = null;

		try {
			entr.begin();
			TypedQuery<Role> query = entityManager.createQuery(
					"SELECT a FROM Role a", Role.class);
			roles = query.getResultList();
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
		}
		return roles;
	}

	public Status findStatus(int statusId) {
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();
		EntityTransaction entr = entityManager.getTransaction();
		Status status = null;
		try {
			entr.begin();
			status = entityManager.find(Status.class, statusId);
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
			e.printStackTrace();
		}
		return status;
	}

	public List<Status> getStatus() {

		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		EntityTransaction entr = entityManager.getTransaction();
		List<Status> status = null;

		try {
			entr.begin();
			TypedQuery<Status> query = entityManager.createQuery(
					"SELECT a FROM Status a", Status.class);
			status = query.getResultList();
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
		}
		return status;
	}

	public Account getAccountByLoginId(String loginId) {

		// Obtains entity manager object
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();

		// -----------Begin transaction-----------
		Account account = null;
		try {
			entr.begin();
			TypedQuery<Account> query = entityManager.createQuery(
					"SELECT a FROM " + Account.class.getName()
							+ " a WHERE a.loginId = :loginId", Account.class)
					.setParameter("loginId", loginId);
			account = query.getSingleResult();
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
		}
		// -----------End transaction-----------

		return account;

	}

	public boolean updateAccount(Account account) {
		EntityManager entityManager = EntityManagerFactoryUtil
				.createEntityManager();

		// Obtains transaction from entity manager
		EntityTransaction entr = entityManager.getTransaction();

		try {
			entr.begin();
			entityManager.merge(account);
			entr.commit();
		} catch (Exception e) {
			entityManager.close();
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
