package csc.fresher.constant;

public final class Constant {

	// Account type
	public static final int ACCOUNT_TYPE_USER_ADMIN = 1;
	public static final int ACCOUNT_TYPE_USER_SUPPORT = 2;
	public static final int ACCOUNT_TYPE_ACCOUNT_ADMIN = 3;
	public static final int ACCOUNT_TYPE_ACCOUNT_SUPPORT = 4;
	public static final int ACCOUNT_TYPE_REPORT_SUPPORT = 5;
	public static final int ACCOUNT_TYPE_CONSUMER = 6;

	// System Status
	public static final int STATUS_ID_NEW = 1;
	public static final int STATUS_ID_ACTIVE = 2;
	public static final int STATUS_ID_DISABLE = 3;
	public static final int STATUS_ID_REMOVEABLE = 4;
}
