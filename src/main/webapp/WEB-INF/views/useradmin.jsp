<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<title>Empty File | Melon - Flat &amp; Responsive Admin Template</title>

	<!--=== CSS ===-->

	<!-- Bootstrap -->
	<link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<!-- jQuery UI -->
	<!--<link href="plugins/jquery-ui/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" />-->
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="plugins/jquery-ui/jquery.ui.1.10.2.ie.css"/>
	<![endif]-->


	<!-- Theme -->
	<link href="lib/assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="lib/assets/css/plugins.css" rel="stylesheet" type="text/css" />
	<link href="lib/assets/css/responsive.css" rel="stylesheet" type="text/css" />
	<link href="lib/assets/css/icons.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="lib/assets/css/fontawesome/font-awesome.min.css">
	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->

	<!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

	<!--=== JavaScript ===-->

	<script type="text/javascript" src="lib/assets/js/libs/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="lib/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

	<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="lib/assets/js/libs/lodash.compat.min.js"></script>

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->

	<!-- Smartphone Touch Events -->
	<!-- <script type="text/javascript" src="plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.move.js"></script>
	<script type="text/javascript" src="plugins/event.swipe/jquery.event.swipe.js"></script> -->

	<!-- General -->
	<script type="text/javascript" src="lib/assets/js/libs/breakpoints.js"></script>
	<script type="text/javascript" src="lib/plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
	<script type="text/javascript" src="lib/plugins/cookie/jquery.cookie.min.js"></script>
	<script type="text/javascript" src="lib/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="lib/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

	<!-- Page specific plugins -->
	<!-- Charts -->
	<!-- <script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>

	<script type="text/javascript" src="plugins/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="plugins/daterangepicker/daterangepicker.js"></script>
	<script type="text/javascript" src="plugins/blockui/jquery.blockUI.min.js"></script> -->

	<!-- Forms -->
	<script type="text/javascript" src="lib/plugins/uniform/jquery.uniform.min.js"></script> <!-- Styled radio and checkboxes -->
	<script type="text/javascript" src="lib/plugins/select2/select2.min.js"></script> <!-- Styled select boxes -->

	<!-- DataTables -->
	<script type="text/javascript" src="lib/plugins/datatables/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="lib/plugins/datatables/tabletools/TableTools.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="lib/plugins/datatables/colvis/ColVis.min.js"></script> <!-- optional -->
	<script type="text/javascript" src="lib/plugins/datatables/DT_bootstrap.js"></script>

	<!-- App -->
	<script type="text/javascript" src="lib/assets/js/app.js"></script>
	<script type="text/javascript" src="lib/assets/js/plugins.js"></script>
	<script type="text/javascript" src="lib/assets/js/plugins.form-components.js"></script>
	<!-- <script type="text/javascript" type="javascript" 
	src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script type="text/javascript" type="javascript"
	src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
	<link rel="stylesheet" type="text/css"
	href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css"> -->

	<script>
	$(document).ready(function(){
		"use strict";

		App.init(); // Init layout and core plugins
		Plugins.init(); // Init all plugins
		FormComponents.init(); // Init all form-specific plugins
		
		var table = $('#myTable').DataTable();
		$('#myTable tbody').on( 'click', 'tr', function () {
			var loginId = $($(this).find('td')[2]).html();
			showModal(loginId);
		});
	});
	
	function showModal(loginId) {
/* 		var MyRows = $('table#myTable').find('tbody').find('tr');
		
		 var current_amount = $(MyRows[account_id -1]).find('td:eq(4)').html();
		 var balance_amount = $(MyRows[account_id -1]).find('td:eq(5)').html(); */
		 $.ajax({
			url: "getAccountDetail?loginId="+loginId,
			type: "POST",
			data: {},
			success: function (result) {
				/* var loginId = result.loginId;
				var password = result.password;
				$("#recipient-name").val(loginId); */
	            /* alert(JSON.stringify(result)); */
	            $("#loginId").val(result.loginId);
	            $("#password").val(result.password);
	            $("#cardNumber").val(result.idCardNumber);
	            $("#firstName").val(result.firstName);
	            $("#midName").val(result.midName);
	            $("#lastName").val(result.lastName);
	            $("#firstPhoneNumber").val(result.firstPhoneNumber);
	            $("#secondPhoneNumber").val(result.secondPhoneNumber);
	            $("#firstEmail").val(result.firstEmail);
	            $("#secondEmail").val(result.secondEmail);
	            $("#firstAddress").val(result.firstAddress);
	            $("#secondAddress").val(result.secondAddress);
	            $("#roles").val(result.role.roleId);
	            $("#status").val(result.status.statusName);
	        },
	        error: function(){
	        	alert("error");
	        }
		})		
	}
	
	
	
	
	
	</script>
</head>
<body>
<!-- Header -->
	<header class="header navbar navbar-fixed-top" role="banner">
		<!-- Top Navigation Bar -->
		<div class="container">

			<!-- Only visible on smartphones, menu toggle -->
			<ul class="nav navbar-nav">
				<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
			</ul>

			<!-- Logo -->
			<a class="navbar-brand" href="index.html">
				<img src="lib/assets/img/logo.png" alt="logo" />
				<strong>ME</strong>LON
			</a>
			<!-- /logo -->

			<!-- Sidebar Toggler -->
			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
				<i class="icon-reorder"></i>
			</a>
			<!-- /Sidebar Toggler -->

			<!-- Top Left Menu -->
			<ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
				
			</ul>
			<!-- /Top Left Menu -->

			<!-- Top Right Menu -->
			<ul class="nav navbar-nav navbar-right">
				<!-- User Login Dropdown -->
				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-male"></i>
						<span class="username">Admin</span>
						<i class="icon-caret-down small"></i>
					</a>
					<ul class="dropdown-menu">
						<li><a href="login.html"><i class="icon-key"></i> Log Out</a></li>
					</ul>
				</li>
				<!-- /user login dropdown -->
			</ul>
			<!-- /Top Right Menu -->
		</div>
		<!-- /top navigation bar -->
	</header> <!-- /.header -->

	<div id="container">
		<div id="sidebar" class="sidebar-fixed">
			<div id="sidebar-content">

				<!--=== Navigation ===-->
				<ul id="nav">
				    <li class="current">
						<a href="editProfileSupport.html">
							<i class="icon-dashboard"></i>
							Profile
						</a>
					</li>
					<li class="current">
						<a href="viewListNewUser.html">
							<i class="icon-dashboard"></i>
							Change Password
						</a>
					</li>
					<li class="current">
						<a href="viewListUpdateUser.html">
							<i class="icon-dashboard"></i>
							Transaction History
						</a>
					</li>
				</ul>
				<!-- /Navigation -->
			</div>
			<div id="divider" class="resizeable"></div>
		</div>
		<!-- /Sidebar -->

		<div id="content">
			<div class="container">
				<!-- Breadcrumbs line -->
				<div class="crumbs">
					
				</div>
				<!-- /Breadcrumbs line -->

				<!--=== Page Header ===-->
				<div class="page-header">
					<div class="page-title">
						<h3>Good morning, Sir!</h3>
					</div>
				</div>
				
				<!-- /Page Header -->
<!-- /no-padding and table-tabletools -->

				<!--=== no-padding and table-colvis ===-->
				
				<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i> Show/ Hide List of New User (<code>no-padding</code> &amp; <code>table-colvis</code>)</h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content no-padding">
								<table id="myTable"
									class="table table-striped table-bordered table-hover table-checkable table-colvis datatable">
									<thead>
										<tr>
											<th class="checkbox-column"><input type="checkbox"
												class="uniform"></th>
											<th>Full Name</th>
											<!-- <th>Role</th> -->
											<th>Login Name</th>
											<th class="hidden-xs">Status Name</th>
											<th>View Info</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="account" items="${listAccountNew}">
											<tr>
												<td class="checkbox-column"><input type="checkbox"
													class="uniform"></td>
												<td>${account.firstName}${account.midName}
													${account.lastName}</td>
												<%-- <td>${account.role.roleName}</td> --%>
												<td>${account.loginId}</td>
												<td class="hidden-xs">${account.status.statusName}</td>
												<td><button id="viewInfo" data-toggle="modal"
														data-target="#exampleModal" class="btn btn-xs btn-primary">View</button></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>			
							</div>
						</div>
					</div>
				</div>


<div class="row">
					<div class="col-md-12">
						<div class="widget box">
							<div class="widget-header">
								<h4><i class="icon-reorder"></i> Managed Table</h4>
								<div class="toolbar no-padding">
									<div class="btn-group">
										<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
									</div>
								</div>
							</div>
							<div class="widget-content" style="display: block;">
								<table id="myTable"
									class="table table-striped table-bordered table-hover table-checkable table-colvis datatable">
									<thead>
										<tr>
											<th class="checkbox-column"><input type="checkbox"
												class="uniform"></th>
											<th>Full Name</th>
											<!-- <th>Role</th> -->
											<th>Login Name</th>
											<th class="hidden-xs">Status Name</th>
											<th>View Info</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="account" items="${listAccountUpdate}">
											<tr>
												<td class="checkbox-column"><input type="checkbox"
													class="uniform"></td>
												<td>${account.firstName}${account.midName}
													${account.lastName}</td>
												<%-- <td>${account.role.roleName}</td> --%>
												<td>${account.loginId}</td>
												<td class="hidden-xs">${account.status.statusName}</td>
												<td><button id="viewInfo" data-toggle="modal"
														data-target="#exampleModal" class="btn btn-xs btn-primary">View</button></td>
											</tr>
										</c:forEach>
									</tbody>
								</table>			
							</div>
						</div>
					</div>
				</div>
				</div> <!-- /.row -->
				<!-- /Page Content -->
				
			</div>
			<!-- /.container -->

		</div>
	
	<!-- /.modal-content Accept/Reject Account-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Account Infomations</h4>
      </div>
      <div class="modal-body">
        <form action="acceptAccount" method="post">
        <%-- <c:forEach var="item" items="${listAccount}"> --%>
          <div class="form-group">
							<label for="recipient-name" class="control-label">LoginId:</label>
							<input type="text" class="form-control" id="loginId" name ="loginId"
								readonly="readonly" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Password</label>
							<input type="text" class="form-control" id="password" name ="password" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Card
								Number</label> <input type="text" class="form-control" id="idCardNumber" name ="idCardNumber" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">First
								Name</label> <input type="text" class="form-control" id="firstName" name ="firstName" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Mid
								Name</label> <input type="text" class="form-control" id="midName" name ="midName" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Last
								Name</label> <input type="text" class="form-control" id="lastName" name ="lastName" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">First
								Phone Number</label> <input type="text" class="form-control"
								id="firstPhoneNumber" name ="firstPhoneNumber" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Second
								Phone Number</label> <input type="text" class="form-control"
								id="secondPhoneNumber" name ="secondPhoneNumber"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">First
								Email</label> <input type="text" class="form-control" id="firstEmail" name ="firstEmail" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Second
								Email</label> <input type="text" class="form-control" id="secondEmail" name ="secondEmail" />
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">First
								Address</label> <input type="text" class="form-control"
								id="firstAddress" name ="firstAddress"/>
						</div>
						<div class="form-group">
							<label for="recipient-name" class="control-label">Second
								Address</label> <input type="text" class="form-control"
								id="secondAddress" name ="secondAddress"/>
						</div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Role</label>
            <input type="text" class="form-control" id="roles" name="role" />
          </div>
          <div class="form-group">
            <label for="recipient-name" class="control-label">Status</label>
            <input type="text" class="form-control" id="status" name="status"/>
          </div>
          <%-- </c:forEach> --%>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-primary" name="accept">Accept</button>
        <button type="Submit" class="btn btn-primary" name="reject">Reject</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </form>
    </div>
    </div>
  </div>
</div>

</body>
</html>
